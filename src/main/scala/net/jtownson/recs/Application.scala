package net.jtownson.recs

import akka.actor.{ActorSystem, Props}
import akka.event.Logging
import akka.io.IO
import net.jtownson.recs.api.RecsApiServiceActor
import spray.can.Http

object Application extends App {

  implicit val system = ActorSystem("recs")
  val log = Logging(system, getClass)

  val service = system.actorOf(Props[RecsApiServiceActor], "recs-api-service-actor")

  IO(Http) ! Http.Bind(service, interface = Settings.host, port = Settings.port)
}
