package net.jtownson.recs

import com.typesafe.config.ConfigFactory

object Settings {
  private val config = ConfigFactory.load()

  lazy val host = config.getString("host")
  lazy val port = config.getInt("port")


  object RecsEngine {
    private val engine = config.getConfig("recs-engine")

    lazy val numOfReqs = engine.getInt("num-recs-per-slot")
    lazy val numOfSlots = engine.getInt("num-slots")

    object Api {
      private val api = engine.getConfig("api")

      lazy val path = api.getString("path")
      lazy val host = api.getString("host")
      lazy val port = api.getInt("port")
    }

  }

}
