package net.jtownson.recs.recs

import akka.actor.{Actor, ActorLogging, Props}
import RecsQuery.{Recommendation, Recommendations, RecsQueryJsonProtocol}
import RecsQueryService.Process
import net.jtownson.recs.Settings
import org.joda.time.DateTime
import spray.client.pipelining._
import spray.httpx.SprayJsonSupport
import spray.routing.RequestContext

import scala.annotation.tailrec
import scala.concurrent.Future
import scala.util.{Failure, Success}
import scala.xml.NodeSeq

class RecsQueryService(requestContext: RequestContext) extends Actor with ActorLogging {

  implicit val system = context.system

  import system.dispatcher

  override def receive: Receive = {
    case Process(subscriberId) =>
      process(subscriberId)
      context.stop(self)
  }

  def process(subscriberId: String) = {
    val times: List[(Long, Long)] = calculateSlotTimes(Settings.RecsEngine.numOfSlots, DateTime.now)
    val listOfResponses: List[Future[NodeSeq]] = times.map { case (start, end) => performRequest(subscriberId)(start, end) }

    val resultFuture = Future.sequence(listOfResponses)

    resultFuture onComplete {
      case Success(xmls) =>
        import RecsQueryJsonProtocol._
        import SprayJsonSupport._
        requestContext.complete(xmls.zip(times).map { case (xml, (_, endMillis)) => convertNode(xml, endMillis) })
      case Failure(ex) =>
        log.error(ex, "Pipeline returned an error.")
        requestContext.complete(ex)
    }

  }

  def performRequest(subscriberId: String): (Long, Long) => Future[NodeSeq] = { (startMillis: Long, endMillis: Long) =>
    val numReqs = Settings.RecsEngine.numOfReqs
    val numSlots = Settings.RecsEngine.numOfSlots
    val apiPath = Settings.RecsEngine.Api.path
    val apiHost = Settings.RecsEngine.Api.host
    val apiPort = Settings.RecsEngine.Api.port
    val completeUrl = s"http://$apiHost:$apiPort/$apiPath"

    val finalCall = s"$completeUrl?num=$numReqs&start=$startMillis&end=$endMillis&subscriber=$subscriberId"

    log.debug("Requesting recommendations for '{}' to [{}]. {} one-hour slots, {} recommendations per slot",
      subscriberId,
      completeUrl,
      numSlots,
      numReqs)


    val pipeline = sendReceive ~> unmarshal[NodeSeq]

    pipeline {
      Get(finalCall)
    }
  }

  def calculateSlotTimes(numSlots: Int, startingTime: DateTime): List[(Long, Long)] = {
    @tailrec
    def loop(n: Int, start: DateTime, acc: List[(Long, Long)]): List[(Long, Long)] = {
      if (n > 0) {
        loop(n - 1, start.plusHours(1), (start.getMillis, start.plusHours(1).getMillis) :: acc)
      } else {
        acc
      }
    }
    loop(numSlots, startingTime, List.empty).reverse // reverse to get the tuples in order
  }

  def convertNode(response: NodeSeq, expire: Long): Recommendations = {
    val recs = (response \ "recommendations").map { node =>
      val uuid = (node \ "uuid").text
      val start = (node \ "start").text.toLong
      val end = (node \ "end").text.toLong
      Recommendation(uuid, start, end)
    }.toList
    Recommendations(recs, expire)
  }
}

object RecsQueryService {

  def props(requestContext: RequestContext) = Props(new RecsQueryService(requestContext))

  case class Process(subscriberId: String)

}
