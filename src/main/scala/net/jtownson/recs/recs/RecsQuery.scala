package net.jtownson.recs.recs

import spray.json._


object RecsQuery {

  case class Recommendation(uuid: String, start: Long, end: Long)

  case class Recommendations(recommendations: List[Recommendation], expiry: Long)

  object RecsQueryJsonProtocol extends DefaultJsonProtocol {
    implicit val recommendationFormat = jsonFormat3(Recommendation.apply)
    implicit val recommendationsFormat = jsonFormat2(Recommendations.apply)

    // Part of the workaround to make the deserialization in integration tests work as expected
    implicit object RecomendationsListFormatter extends RootJsonFormat[List[Recommendations]] {
      def write(list: List[Recommendations]) = JsArray(list.map(_.toJson).toVector)
      def read(value: JsValue): List[Recommendations] = value match {
        case JsArray(elements) => elements.map(_.convertTo[Recommendations])(collection.breakOut)
        case x => deserializationError("Expected List as JsArray, but got " + x)
      }
    }
  }

}
