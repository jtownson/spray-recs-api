package net.jtownson.recs.api

import akka.actor.Actor
import net.jtownson.recs.recs.RecsQueryService
import spray.routing.HttpService

class RecsApiServiceActor extends Actor with RecsApiService {
  def actorRefFactory = context

  def receive = runRoute(recsApiRoot)
}

trait RecsApiService extends HttpService {

  val recsApiRoot =
    get {
      pathPrefix("personalised" / Segment) { subscriber =>
        ctx =>
          val recsEngineService = actorRefFactory.actorOf(RecsQueryService.props(ctx))
          recsEngineService ! RecsQueryService.Process(subscriber)
      }
    }

}
