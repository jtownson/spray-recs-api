package net.jtownson.recs

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.client.WireMock._

class RecsEngineMock() {

  val port: Int = 8080
  val server = new WireMockServer(port)
  val client = new WireMock("127.0.0.1", port)

  def start() {
    server.start()
  }

  def stop() {
    server.stop()
  }

  def reset() {
    client.resetMappings()
  }


  def willReturnRecommendations(body: String): Unit = {
    client.register(get(urlPathEqualTo("/recs/personalised")).willReturn(aResponse().withStatus(200).withHeader("Content-Type", "application/xml").withBody(body)))
  }
}
