package net.jtownson.recs

import java.util.UUID

import net.jtownson.recs.api.RecsApiService
import net.jtownson.recs.recs.RecsQuery._
import org.scalatest.{BeforeAndAfterAll, BeforeAndAfterEach, FreeSpec, Matchers}
import spray.http.{MediaTypes, StatusCodes}
import spray.testkit.ScalatestRouteTest

import scala.concurrent.duration._


/**
 * This Specification represents a basic Integration Test Suite.
 * It depends on an external working service to provide the recommendations, but the specific service used is configurable
 * in a test version of application.conf.
 */
class ApiIntegrationTestSpec extends FreeSpec
with BeforeAndAfterAll
with ScalatestRouteTest
with RecsApiService
with Matchers {
  val numOfReqs = Settings.RecsEngine.numOfReqs
  val numOfSlots = Settings.RecsEngine.numOfSlots
  val recsEngineMock = new RecsEngineMock
  def actorRefFactory = system

  val start = 0
  val end = Long.MaxValue

  override protected def beforeAll(): Unit = {
    recsEngineMock.start()
    recsEngineMock.willReturnRecommendations(
        s"<?xml version='1.0' encoding='UTF-8' standalone='yes'?> " +
        s"<recommendations>\n" +
        s"    <recommendations>\n" +
        s"      <uuid>${UUID.randomUUID().toString}</uuid> <start>${start}</start> <end>${end}</end>\n" +
        s"    </recommendations>\n" +
        s"    <recommendations>\n" +
        s"      <uuid>${UUID.randomUUID().toString}</uuid> <start>${start}</start> <end>${end}</end>\n" +
        s"    </recommendations>\n" +
        s"    <recommendations>\n" +
        s"      <uuid>${UUID.randomUUID().toString}</uuid> <start>${start}</start> <end>${end}</end>\n" +
        s"    </recommendations>\n" +
        s"  </recommendations>"
    )
  }

  override protected def afterAll(): Unit = {
    recsEngineMock.stop()
  }

  // work around to the fact that I could not find a way to
  // make 'response[List[Recommendations]]' work!
  private def deserializeRecsList(response: String): List[Recommendations] = {
    val tmp = responseAs[String]
    import spray.json._
    import DefaultJsonProtocol._
    import RecsQueryJsonProtocol._
    val json = tmp.parseJson
    json.convertTo[List[Recommendations]]
  }

  // This relaxes the timeout on route testing
  implicit val t = RouteTestTimeout(5 seconds)
  "Given a RecsApiService" - {
    "when called with no-parameters GET /personalised" - {
      "should return Not Found response" in {
        Get("/personalised") ~> sealRoute(recsApiRoot) ~> check {
          status should equal(StatusCodes.NotFound)
        }
      }
    }

    "when called with POST /personalised/mySubscriber" - {
      "should return Method Not Allowed response" in {
        Post("/personalised/mySubscriber") ~> sealRoute(recsApiRoot) ~> check {
          status should equal(StatusCodes.MethodNotAllowed)
        }
      }
    }

    "when called with POST /personalised and argument 'mySubscriber'" - {
      "should return Method Not Allowed response" in {
        Post("/personalised", "mySubscriber") ~> sealRoute(recsApiRoot) ~> check {
          status should equal(StatusCodes.MethodNotAllowed)
        }
      }
    }

    "when the EXTERNAL Recommendation Engine Endpoint is available" - {
      "when called with GET /personalised/mySubscriber" - {
        s"should return $numOfSlots slots of recommendations, each expiring 1 hour apart" in {
          Get("/personalised/mySubscriber") ~> recsApiRoot ~> check {
            status should equal(StatusCodes.OK)
            mediaType should equal(MediaTypes.`application/json`)

            val recs = deserializeRecsList(responseAs[String]).sortBy(_.expiry)

            // This checks that each recommendation has the expected requests
            recs.size should equal(numOfSlots)

            // This checks that each recommendation is 1-hour apart from the other
            val expiries = recs.map(_.expiry)
            expiries.zip(expiries.tail).map { case (x, y) => y - x }.foreach(millis => millis should equal(60 * 60 * 1000))
          }
        }

        s"should return slots containing $numOfReqs recommendations each" in {
          Get("/personalised/mySubscriber1") ~> recsApiRoot ~> check {
            status should equal(StatusCodes.OK)
            mediaType should equal(MediaTypes.`application/json`)

            val recs = deserializeRecsList(responseAs[String])

            // This checks that each recommendation has the expected requests
            recs.foreach { recommendation => recommendation.recommendations.size should equal(numOfReqs) }
          }
        }
      }
    }

  }
}
